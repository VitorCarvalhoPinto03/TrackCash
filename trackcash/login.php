<html>

<head>
    <title>TrackCash</title> 
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="style.css">
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
</head>

<body>
    <div class="dir_div-log">
        <div class="dir_content-log">
        <img src=TRACKCASH.png>
        <h1>Fazer login no TrackCash:</h1>
        <form action="ver_log.php" method="POST">
            <input type="text" placeholder="Email" name ="email" id="email" required><br/><br/>
            <input type="password" placeholder="Senha" name="senha" id="senha" required><br/><br/>
            <button id="btnConfirma">Acessar o sistema</button>
        </form>
        <p><a id="abrir" href="#">Recuperar senha</a></p>
        <p><a href="cadastro.html">Cadastre-se</a></p>
        <input type="checkbox" name="lembrar" id="lembrar">Lembre-me<br/>
    </div>
</div>


    <div class="modalBox" id="modalBox" >
        <div class="modalContent" id="modalContent">
            <a id='fechar' class='fas fa-times'></a>
            <h1>Recuperar Senha</h1>
            <p>Digite abaixo seu email, que enviaremos para você um link para redefinir sua senha!</p>
            <input type="text" placeholder="Digite se email" id="rec_email" name="rec_email"><br/><br/>
            <button id="btnRec">Enviar link de recuperação</button>
        </div>
    </div>


    <div class="modalBoxRec" id="modalBoxRec" >
        <div class="modalContentRec" id="modalContentRec">
        <a id='fecharRec' class='fas fa-times'></a>    
        <img src=shieldSuccess.png>
            <h1>Instruções enviadas!</h1>
            <p>As instruções foram enviadas para o seu email com sucesso!</p>
            <p> Caso o email não seja encontrado na sua caixa de entrada,
                verifique a caixa de spam/lixo eletrônico.</p>
            <button id="btnGoLogin">Realizar login</button>
        </div>
    </div>

    <div class="modalBoxRed" id="modalBoxRed" >
        <div class="modalContentRed" id="modalContentRed">
            <a id='fecharRed' class='fas fa-times'></a>
            <h1>Redefinir senha</h1>
            <p>Sua nova senha deve conter, no mínimo, 8 caracteres,
                e não pode ser igual a sua senha antiga!</p>
                <input type="password" placeholder="Digite sua nova senha" id="newPass" name="newPass" required><br/><br/>
                <input type="password" placeholder="Confirme sua nova senha" id="cnewPass" name="cnewPass" required><br/><br/>
                <button id="btnRedSenha">Redefinir senha</button>
        </div>
    </div>





<footer> 
    <script src="jquery.js"></script>
    <script src="script.js"></script>
</footer>
</body>
</html>
